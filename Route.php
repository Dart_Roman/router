<?php
namespace DartRoman;

class Route
{
    /**
     * Префикс пространства имен для контроллеров
     * @var string
     */
    protected static $prefix = 'Controllers';

    /**
     * Разделитель между префиксом и названием класса
     * @var string
     */
    protected static $separator = '\\';

    /**
     * Путь до папки с маршрутами
     * @var string
     */
    protected static $routesDirectory = 'routes';

    private static $routes = [
        'GET'    => [],
        'POST'   => [],
        'PUT'    => [],
        'DELETE' => [],
    ];

    private static function getRoutes()
    {
        foreach (scandir('routes') as $element) {
            if ($element !== '.' && $element !== '..') {
                include_once self::$routesDirectory . '/' . $element;
            }
        }
    }

    private static function prepareRule($rule, $match = [])
    {
        $rule = preg_replace('/{(\w+)}/', '(?P<$1>)', $rule);

        foreach ($match as $key => $value) {
            $rule = preg_replace('/(\<' . $key . '\>)/', '$1' . $value . '', $rule);
        }

        return $rule;
    }

    public static function get($controller, $rule, $match = [])
    {
        $rule = self::prepareRule($rule, $match);

        self::$routes['GET'][$controller] = $rule;
    }

    public static function post($controller, $rule, $match = [])
    {
        $rule = self::prepareRule($rule, $match);

        self::$routes['POST'][$controller] = $rule;
    }

    public static function put($controller, $rule, $match = [])
    {
        $rule = self::prepareRule($rule, $match);

        self::$routes['PUT'][$controller] = $rule;
    }

    public static function delete($controller, $rule, $match = [])
    {
        $rule = self::prepareRule($rule, $match);

        self::$routes['DELETE'][$controller] = $rule;
    }

    public static function start()
    {
        self::getRoutes();

        $url    = str_replace(SUBDOMAIN, '', $_SERVER['REDIRECT_URL']) ?: '/';
        $method = $_SERVER['REQUEST_METHOD'];

        foreach (self::$routes[$method] as $action => $rule) {
            if (preg_match('#^' . $rule . '$#', $url, $data)) {
                self::go($action, $data);

                exit();
            }
        }

        header("HTTP/1.1 404 Not Found");

        exit();
    }

    private static function go(string $info, array $data)
    {
        $info = explode('@', $info);
        $controller = self::$prefix . self::$separator . $info[0];
        $action = $info[1];
        $class  = new $controller;

        $values = self::getParams($controller, $action, $data);

        call_user_func_array([$class, $action], $values);
    }

    private static function getParams(string $controller, string $action, array $data): array
    {
        $class  = new \ReflectionClass($controller);
        $method = $class->getMethod($action);
        $params = $method->getParameters();

        $values = [];

        foreach ($params as $param) {
            $values[] = isset($data[$param->name]) ? $data[$param->name] : null;
        }

        return $values;
    }
}
